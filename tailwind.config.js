/** @type {import('tailwindcss').Config} */
module.exports = {
   content: [
    "./src/**/*.{js,jsx,ts,tsx}",
  ],
  theme: {
    extend: {
      backgroundImage : {
        'bgimg-1' : 'linear-gradient(234deg, #ED3A9B 16.81%, #A63DFA 50.74%, #00F 92.14%)'
      }
    },
  },
  plugins: [],
}
