const Clipboard = (value) => {
    navigator.clipboard.writeText(value)
}

export default Clipboard;